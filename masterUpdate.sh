#!/bin/bash

# Passed arguments
prod_project=$1
#test_project=$2
commit_message=$3
# Pull from dev
git pull origin dev
# Sort files
mv "$1/src/" "/src/"
rm -rf "$1"
#rm -rf "$2"
# Create the library
mkdir "/target/"
mkdir "/bin/"
javac -d "/bin/" "/src/*.java"
jar -cf "/target/$1.jar" "/bin/*.class"
# Push to master
git add '/src' '/bin' '/target'
git commit -m "$3"
git push -u origin master